const fs = require('fs');
const express = require('express');
const path = require('path');
const url = require('url');

const EXTENTIONS = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
const filesDir = 'files';

function checkExtention(ext) {
  return EXTENTIONS.includes(ext);
}

function createFile(req, res, next) {
  const filename = path.join(__dirname, `/files/${req.body.filename}`);
  const content = req.body.content;
  const extention = path.extname(`./files/${req.body.filename}`).split('.')[1];

  if (req.body.filename === '') {
    res.status(400).send({ "message": "File has no name" });
  } else if (extention === undefined) {
    res.status(400).send({ "message": "File has no extention" });
  } else if (!checkExtention(extention)) {
    res.status(400).send({ "message": `Extention "${extention}" isn't supported` });
  } else {
    fs.writeFile(filename, content, 'utf8', (err) => {
      if (err) {
        next(err);
      } else {
        res.status(200).send({ "message": "File created successfully" });
      };
    });
  }
}

function getFiles(req, res, next) {
  fs.readdir(filesDir, (err, data) => {
    if (err) {
      next(err);
    } else {
      res.status(200).send({
        "message": "Success",
        "files": data
      });
    };
  });
}

function getFile(req, res, next) {
  const filename = req.params.filename;
  const extention = path.extname(`./files/${filename}`).split('.')[1];
  console.log('filename  ' + filename);
  console.log('extention  ' + extention);

  if (filename === '') {
    res.status(400).send({ "message": "File has no name" });
  } else if (extention === undefined) {
    res.status(400).send({ "message": "File has no extention" });
  } else if (!checkExtention(extention)) {
    res.status(400).send({ "message": `Extention <${extention}> isn't supported` });
  } else {
    fs.access(__dirname + `/files/${filename}`, (err) => {
      if (err) {
        res.status(400).send('File does not exist');
      } else {
        const content = fs.readFileSync(`./files/${filename}`, 'utf8');
        const stats = fs.statSync(`./files/${filename}`);
        const uploadedDate = stats.birthtime;

        res.status(200).send({
          "message": "Success",
          "filename": filename,
          "content": content,
          "extension": extention,
          "uploadedDate": uploadedDate
        });
      }
    });
  }
}

function editFile(req, res, next) {
  const filename = url.parse(req.url, true).path.slice(2);

  fs.access(__dirname + `/files/${filename}`, (err) => {
    if (err) {
      res.status(400).send('File does not exist');
    } else {
      const content = req.body.content;
      fs.writeFile(`/files/${filename}`, content, 'a', (err) => {
        if (err) {
          next(err);
        } else {
          res.status(200).send({ "message": "File edited successfully" });
        }
      });
    }
  })
}

const deleteFile = (req, res, next) => {
  const filename = req.body.filename;
  const content = req.body.content;

  if (!fs.existsSync(`/files/${filename}`)) {
    throw ('File does not exist');
  } else {
    fs.unlink(`/files/${filename}`, (err) => {
      if (err) {
        next(err);
      } else {
        res.status(200).send({ "message": "File deleted successfully" });
      }
    });
  };
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
};
